package com.te.practise.streams3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntermediateOperations {

	public static void main(String[] args) {
		List<Product> productsList = new ArrayList<Product>();
		productsList.add(new Product(1, "HP Laptop", 25000.00));
		productsList.add(new Product(2, "Dell Laptop", 30000.00));
		productsList.add(new Product(3, "Lenevo Laptop", 28000.00));
		productsList.add(new Product(4, "Sony Laptop", 28000.00));
		productsList.add(new Product(5, "Apple Laptop", 90000.00));
		productsList.add(new Product(5, "Apple Laptop", 90000.00));

		// count(): counting the elements that are passed by the stream
		long count = productsList.stream().distinct().count();
		System.out.println(count);

		// map() : is used apply function on the elements and pass it to the next
		// operation
		List<Integer> transformedList = List.of(1, 2, 3, 4).stream().map(a -> a * a).collect(Collectors.toList());
		System.out.println("Transformed list using map :" + transformedList);

		// flatMap(): is used to merge the 2 or more list into 1 list
		List<List<Integer>> lists = List.of(List.of(1, 2), List.of(3, 4), List.of(5, 6));
		List<Integer> mergedList = lists.stream().flatMap(a -> a.stream()).collect(Collectors.toList());
		System.out.println("Merged list using flat map :" + mergedList);

		/*
		 * filter() : It is used to filter the stream by using Predicate Function and
		 * pass them to next operation based on the result given by test() of Predicate
		 */
		List<Integer> filteredList = mergedList.stream().filter(e -> e > 2).collect(Collectors.toList());
		System.out.println("filtered List using filter : " + filteredList);

		/*
		 * limit() : It is used to the restrict the number of elements to be passed to
		 * next operation
		 */
		List<Integer> limitedList = mergedList.stream().limit(2).collect(Collectors.toList());
		System.out.println("Limited the list using limit() :" + limitedList);

		/*
		 * skip(): It used to skip the elements passing to the next operation it we use
		 * 2 as a parameter then it skips first 2 elements of stream and pass other
		 * elements to next operation
		 */
		List<Integer> skippedList = mergedList.stream().skip(1).collect(Collectors.toList());
		System.out.println("skipped the elements using skip() :" + skippedList);

		/*
		 * peek(): it will take the stream of elements and pass it to the next
		 * operation, it will take consumer function that applies on it but doesn't
		 * modify the values the purpose of this method is to support debugging so that
		 * we can the understand the flow of the elements in stream
		 */
		mergedList.stream().skip(1).peek(e -> System.out.println(e)).collect(Collectors.toList());

		/*
		 * Distinct() : it is used to pass the unique elements to the next operation
		 */
		List<Integer> uniqueList = List.of(1, 2, 3, 2, 2, 1).stream().distinct().collect(Collectors.toList());
		System.out.println("unique list using distinct() :" + uniqueList);

		/*
		 * sorted() : it sorts the elements in the natural order and pass them to next
		 * operation
		 */
		List<Integer> naturalOrder = List.of(9, 8, 7, 6, 5).stream().sorted().collect(Collectors.toList());
		System.out.println("Natural order :" + naturalOrder);

		/*
		 * sorted(comparator) : it sorts the elements in the user defined order by
		 * taking using comparator and pass them to next operation
		 */
		List<Integer> userDefinedOrder = List.of(1, 2, 3, 3, 5).stream().sorted((e1, e2) -> e2.compareTo(e1))
				.collect(Collectors.toList());
		System.out.println("UserDefing order :" + userDefinedOrder);

		/*
		 * allMatch() : it is used to or check the elements in our list based on
		 * condition it will check the whether all elements satisfying the given
		 * condition if satisfied return true else false
		 */
		boolean allMatch = productsList.stream().allMatch(e -> e.getPrice() > 35000.00);
		System.out.println(allMatch);

		/*
		 * anyMatch() : it is used to or check the elements in our list based on
		 * condition it will check the whether any of elements satisfying the given
		 * condition if satisfied return true else false
		 */
		boolean anyMatch = productsList.stream().anyMatch(e -> e.getPrice() > 35000.00);
		System.out.println(anyMatch);

		/*
		 * findFirst() : it returns the first element of the stream
		 */
		Optional<Product> findFirst = productsList.stream().findFirst();
		System.out.println("findFirst :" + findFirst.get());

		/*
		 * findAny() : it returns the any one element of the stream, element might
		 * differ for every execution
		 */
		Optional<Product> findAny = productsList.stream().findAny();
		System.out.println("findAny :" + findAny.get());

		/*
		 * max(Comparator) : it returns the max element in the stream
		 */
		Optional<Product> max = productsList.stream().max((e1, e2) -> e1.getPrice().compareTo(e2.getPrice()));
		System.out.println("max :" + max.get());

		/*
		 * min(Comparator) : it returns the max element in the stream
		 */
		Optional<Product> min = productsList.stream().min((e1, e2) -> e1.getPrice().compareTo(e2.getPrice()));
		System.out.println("min :" + min.get());

		/*
		 * toArray() : it is used to convert the stream of elements into array
		 */
		Object[] array = productsList.stream().toArray();
		System.out.println("Arrays :" + Arrays.toString(array));

		/*
		 * reduce(accumulator) : it performs action based on the accumulator value
		 */
		Optional<Integer> reduceAccumulator = List.of(1, 2, 3, 4).stream().reduce((a, b) -> a + b);
		System.out.println("Reduce using accumulator :" + reduceAccumulator.get());

		/*
		 * reduce using identity and accumulator identity: initial value
		 */
		Integer reduceWithIdentityAccumulator = List.of(1, 2, 3, 4).stream().reduce(10, (a, b) -> a + b);
		System.out.println("reduce using identiy and accumulator :" + reduceWithIdentityAccumulator);

		/*
		 * reduce operation using Identity , accumulator and combiner.
		 */
		Integer reduceWithIdentityAccumulatorCombiner = List.of(1, 2, 3, 4).parallelStream().reduce(0, (a, b) -> a + b,
				(a, b) -> a + b);
		System.out.println("reduce using Identity Accumulator Combiner :" + reduceWithIdentityAccumulatorCombiner);

		/*
		 * used to return the sum of all the elements
		 */
		System.out.println("Average :" + List.of(1, 2, 3, 4).stream().reduce((a, b) -> a + b / 2).get());

		/*
		 * Creating stream object
		 */
		Stream<Integer> streamObject = Stream.of(1, 2, 3, 4, 5, 6);
		/*
		 * converting stream to array or non stream object
		 */
		Integer[] array2 = streamObject.toArray(Integer[]::new);
		System.out.println("Integer array :" + Arrays.toString(array2));

		Stream<String> of = Stream.of("rakesh", "rajesh");

		String[] array3 = of.toArray(String[]::new);
		System.out.println("String Array :" + Arrays.toString(array3));

	}

}
