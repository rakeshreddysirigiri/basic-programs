package com.te.practise.streams3;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectorsClass {

	public static void main(String[] args) {
		List<Product> productsList = new ArrayList<Product>();
		productsList.add(new Product(1, "HP Laptop", 25000.00));
		productsList.add(new Product(2, "Dell Laptop", 30000.00));
		productsList.add(new Product(3, "Lenevo Laptop", 28000.00));
		productsList.add(new Product(4, "Sony Laptop", 28000.00));
		productsList.add(new Product(5, "Apple Laptop", 90000.00));
		productsList.add(new Product(5, "Apple Laptop", 90000.00));

		// finding the total price of the products using collectors summing method
		Double totalPrice = productsList.stream().collect(Collectors.summingDouble(e -> e.getPrice()));
		System.out.println(totalPrice);

		// finding the average of the products price
		Double averageprice = productsList.stream().collect(Collectors.averagingDouble(e -> e.getPrice()));
		System.out.println(averageprice);

		// count the number of elements in collection type
		long count = productsList.stream().collect(Collectors.counting());
		System.out.println(count);

		// groupingBy is used to group the elements based on the criteria into a Map
		Map<Double, List<Product>> grouping = productsList.stream().collect(Collectors.groupingBy(Product::getPrice));
		System.out.println(grouping);

		System.out.println("--------------------------------------------");
		// converting the list to linkedHashMap( keyMapper,valueMapper,mergeFunction,map
		// supplier
		// map
		// object to store the data)
		LinkedHashMap<Integer, Double> collectToLinkedHashMap = productsList.stream()
				.sorted((e1, e2) -> e2.getPrice().compareTo(e1.getPrice())).collect(
						Collectors.toMap(Product::getProductId, Product::getPrice, (e1, e2) -> e2, LinkedHashMap::new));
		System.out.println(collectToLinkedHashMap);
		System.out.println("--------------------------------------------");

		// converting list to map using product as key and id as value
		Map<Product, Integer> collectToMap = productsList.stream()
				.collect(Collectors.toMap(Function.identity(), Product::getProductId));
		System.out.println(collectToMap);

	}
}
