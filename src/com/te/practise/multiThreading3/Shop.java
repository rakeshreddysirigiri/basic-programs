package com.te.practise.multiThreading3;

public class Shop {

	int n;
	boolean available = false;

	public synchronized void addItem(int n) throws InterruptedException {
		this.n = n;
		if (available == true) {
			wait();
		}
		System.out.println(Thread.currentThread());
		System.out.println("Added item number : " + n);
		available = true;
		notify();

	}

	public synchronized void getItem(int n) throws InterruptedException {
		this.n = n;
		if (available == false) {
			wait();
		}
		System.out.println(Thread.currentThread());
		System.out.println("Received item number : " + n);
		available = false;
		notify();

	}
}
