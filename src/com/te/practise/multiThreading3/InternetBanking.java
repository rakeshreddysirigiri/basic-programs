package com.te.practise.multiThreading3;

public class InternetBanking extends Thread {

	WithDraw withDraw;

	public InternetBanking(WithDraw withDraw) {
		this.withDraw = withDraw;
	}

	public void run() {
		withDraw.withDrawAmount(2000);
	}
}
