package com.te.practise.multiThreading3;

public class Main {

	public static void main(String[] args) {
		Shop shop = new Shop();
		Owner owner = new Owner(shop);
		Customer customer = new Customer(shop);
		owner.setName("owner");
		customer.setName("customer");
		owner.start();
		customer.start();
	}
}
