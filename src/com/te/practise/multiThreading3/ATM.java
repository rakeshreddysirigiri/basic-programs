package com.te.practise.multiThreading3;

public class ATM extends Thread {

	WithDraw withDraw;

	public ATM(WithDraw withDraw) {
		this.withDraw = withDraw;
	}

	public void run() {
		withDraw.withDrawAmount(5000);
	}
}
