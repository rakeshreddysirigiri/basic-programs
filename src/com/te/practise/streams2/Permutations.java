package com.te.practise.streams2;

public class Permutations {
	public static void main(String[] args) {
		String s = "ABC";
		char[] a = s.toCharArray();
		for (int i = 0; i < a.length; i++) {
			String s1 = "" + a[i];
			for (int j = 0; j < a.length; j++) {
				char ch = a[j];
				if (i != j) {
					s1 = s1 + ch;
				}
			}
			System.out.println(s1);
		}
	}
}
