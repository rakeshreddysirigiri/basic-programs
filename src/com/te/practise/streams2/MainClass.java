package com.te.practise.streams2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MainClass {

	public static void main(String[] args) {

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee("Ty01", "Peter", 54946.00));
		employees.add(new Employee("Ty02", "mark", 54865.00));
		employees.add(new Employee("Ty03", "Andrew", 54879.00));

		Map<String, Employee> collect = employees.stream().sorted((e1, e2) -> e1.getSalary().compareTo(e2.getSalary()))
				.collect(Collectors.toMap(Employee::getEmpId, Function.identity(), (e1, e2) -> e1, LinkedHashMap::new));

		collect.entrySet().stream().forEach(System.out::println);
		
		LinkedHashSet<E>
		
		// find duplicates in a list using streams
		List<Integer> intList = new ArrayList<Integer>();
		intList.add(1);
		intList.add(5);
		intList.add(6);
		intList.add(5);
		intList.add(6);
		intList.add(4);
		intList.add(6);
		intList.add(6);
		Set<Integer> set = new HashSet<Integer>();
		intList.stream().filter(e->Collections.frequency(intList,e)>1).distinct().forEach(e->System.out.println(e));
		System.out.println("------------------------------------------------------------");
		List<Integer> list = intList.stream().filter(e->!set.add(e)).collect(Collectors.toList());
		Integer integer = intList.stream().max((e1,e2)->e1-e2).get();
		Integer integer1 = intList.stream().max((e1,e2)->e2-e1).get();
		list.forEach(System.out::println);
		System.out.println("++++++++++++"+integer1);
		System.out.println("--------"+integer);
		
		int n1=0;
		int n2=1;
		int n3;
		System.out.print(n1+" "+n2+" ");
		for(int i=3;i<=10;i++) {
			 n3=n1+n2;
			System.out.print(n3+" ");
			n1=n2;
			n2=n3;
		}
		

	}
}
