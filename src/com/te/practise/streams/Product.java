package com.te.practise.streams;

import java.util.LinkedHashSet;
import java.util.Set;

public class Product {

	private String name;

	private String category;

	private Double price;

	private Set<Order> orders = new LinkedHashSet<Order>();

	public Product() {
		super();
	}

	public Product(String name, String category, Double price, Set<Order> orders) {
		super();
		this.name = name;
		this.category = category;
		this.price = price;
		this.orders = orders;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Set<Order> getOrders() {
		return orders;
	}

	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	
	
}
