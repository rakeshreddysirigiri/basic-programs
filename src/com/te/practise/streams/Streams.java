package com.te.practise.streams;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

abstract public class Streams {

	public static void main(String[] args) {
		
// creating a stream 	

		// using Arrays class
		String[] arr = new String[] { "abc", "bba", "cca", "dda" };
		Stream<String> stream = Arrays.stream(arr);

		// using stream
		Stream<String> string = Stream.of("a", "b", "c", "d");
		string.forEach(System.out::print);

		// using builder
		Stream.Builder<String> build = Stream.builder();
		build.add("a");
		build.add("b");
		build.build();

		// concat 2 streams using into one stream

// Intermediate operations on stream

		// filter
		Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 2, 4, 2, 6, 2, 5, 6, 4, 3, 5);
		List<Integer> primes = intStream.filter(e -> {
			for (int i = 2; i < e; i++) {
				if (e % i == 0) {
					return false;
				}
			}
			return true;

		}).collect(Collectors.toList());
		System.out.println(primes);
		// map used to transform the stream element and produce a new stream element
		Stream<Integer> map = primes.stream().map(e -> e * 10);
		map.forEach(System.out::print);
		System.out.println();

		// distinct-used to get unique elements from the stream
		primes.stream().distinct().forEach(System.out::print);

		// peek
		primes.stream().map(e -> e * e).peek(e -> System.out.println("squared " + e)).forEach(System.out::println);

		// sorted
		primes.stream().sorted((a, b) -> b - a).forEach(System.out::print);

//Terminal operations

		// all match
		boolean allMatch = primes.stream().allMatch(e -> e > 2);
		System.out.println("allMactch " + allMatch);

		// anyMatch
		boolean anyMatch = primes.stream().anyMatch(e -> e > 2);
		System.out.println("anyMatch " + anyMatch);

		// noneMatch
		boolean noneMatch = primes.stream().noneMatch(e -> e > 2);
		System.out.println("noneMatch " + noneMatch);

		Integer reduce = primes.stream().reduce(0, (a, b) -> a + b);
		System.out.println(reduce);

		Optional<Integer> reduce2 = primes.stream().reduce((a, b) -> a * b);
		System.out.println(reduce2.get());

		Optional<Integer> reduce3 = primes.stream().reduce(Integer::sum);
		System.out.println(reduce3.get());

		List<Employee> employee = new ArrayList<Employee>();
		employee.add(new Employee("1", "Rakesh", 26826.00));
		employee.add(new Employee("2", "rkgjnkjn", 51541.00));
		employee.add(new Employee("3", "okofmokwm", 65166.21));
		employee.add(new Employee("4", "mfkoerm", 68451.00));
		employee.add(new Employee("5", "oegnnvjvn", 655464.00));
		double asDouble = employee.stream().parallel().map(Employee::getEmpSalary).peek(e -> System.out.println(e))
				.mapToDouble(i -> i).max().getAsDouble();
		System.out.println(asDouble);
		double reduce4 = employee.stream().parallel().map(e -> e.getEmpSalary()).mapToDouble(i -> i).reduce(0,
				(a, b) -> a + b);
		System.out.println(reduce4);

		employee.stream().parallel().filter(e -> e.getEmpId().equals("1"))
				.forEach(e -> e.setEmpSalary(e.getEmpSalary() * 2));
		employee.forEach(System.out::println);

		Map<String, List<String>> mapStates = new HashMap<String, List<String>>();

		mapStates.put("Karnataka", List.of("Bengaluru", "Mysore"));
		mapStates.put("Telangana", List.of("Hyderabad", "Medak"));
		mapStates.put("Tamilnadu", List.of("pondicherry", "Chennai"));
		mapStates.put("Andhrapradesh", List.of("Karnulu", "kadapa"));

		String state = getState("Karnulu", mapStates);

		String state2 = getState("Bengaluru", mapStates);

		String state3 = getState("Chennai", mapStates);

		System.out.println(state + " " + state2 + " " + state3);
		List<Order> order = new ArrayList<Order>();

		List<Product> products = new ArrayList<Product>();
		// list of products that belongs to baby with price greater than 100
		List<Product> collect = products.stream().filter(e -> e.getCategory().contains("Books"))
				.filter(e -> e.getPrice() > 100).collect(Collectors.toList());

		// Obtain a list of order with products belong to category �Baby�
		List<Order> collect2 = order.stream()
				.filter(e->e.getProducts()
						.stream()
						.anyMatch(p->(p.getCategory()).equalsIgnoreCase("Baby"))
						)
				.collect(Collectors.toList());

		// Obtain a list of product with category = �Toys� and then apply 10% discount
		products.stream().filter(e -> e.getCategory().equals("Toys")).forEach(e -> e.setPrice(e.getPrice() - 100 / 10));

		// Obtain a list of products ordered by customer of tier 2 between 01-Feb-2021
		// and 01-Apr-2021
       
		order.stream().filter(e->e.getCustomer().getTier()==2)
		.filter(e->e.getOrderDate().compareTo(LocalDate.of(2021, 1, 31)));
		
		
		

	}

	public static String getState(String cityName, Map<String, List<String>> mapStates) {
		Set<Entry<String, List<String>>> collect = mapStates.entrySet().stream()
				.filter(e -> e.getValue().contains(cityName)).collect(Collectors.toSet());
		return collect.stream().findFirst().get().getKey();
	}
	
	 

}
