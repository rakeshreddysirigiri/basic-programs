package com.te.practise.streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Duplicates {
  public static void main(String[] args) {
	
	  List<Employee> employees = new ArrayList<Employee>();
	  employees.add(new Employee("ty001","rakesh",12354.00));
	  employees.add(new Employee("ty002","rakesh",12354.00));
	  employees.add(new Employee("ty003","venky",12354.00));
	  employees.add(new Employee("ty004","vinay",12354.00));
	  employees.add(new Employee("ty005","chandra",12354.00));
	  employees.add(new Employee("ty006","rakesh",12354.00));
	  
	  Map<String, Employee> collect = employees.stream().
			  collect(Collectors.toMap(Employee::getEmpName,Function.identity(),(e1,e2)->e1));
     System.out.println(collect);
     
     employees.stream()
  }
}
