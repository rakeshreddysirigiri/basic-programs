package com.te.practise.streams;

class AA {

	public void aa() {
		System.out.println("aa");
	}
}

class BB extends AA {

	public void aa() {
		System.out.println("bb");
	}
}

class CC extends AA {
	public void aa() {
		System.out.println("aa");
	}
}

public class Abc {

	public static void main(String[] args) {
		AA a = new BB();
		a.aa();
	}
}
