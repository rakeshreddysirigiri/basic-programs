package com.te.practise.streams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hashmap {
	public static void main(String[] args) {

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		map.put("student1", List.of("DBMS", "NETWROKING"));
		map.put("student2", List.of("DBMS", "NETWROKING", "Java"));
		map.put("student3", List.of("NETWROKING", "Java"));

		map.entrySet().stream().filter(e -> e.getValue().contains("NETWROKING")).forEach(System.out::println);
	}
}
