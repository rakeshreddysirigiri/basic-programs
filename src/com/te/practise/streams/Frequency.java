package com.te.practise.streams;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

public class Frequency {

	public static void main(String[] args) {

		Integer[] a = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 4, 3, 2, 1 };

		Arrays.asList(a).stream().distinct()
				.forEach(e -> System.out.println(Collections.frequency(Arrays.asList(a), e) + " " + e));
		;

		Map<Integer, Long> collect = Arrays.asList(a).stream()
				.collect(Collectors.groupingBy(e -> e, Collectors.counting()));
		System.out.println(collect);

		panGram();

	}

	public static void panGram() {

		String s = "The quick brown fox jumps over the lazy dog";

		int[] a = new int[256];

		for (int i = 0; i < s.length(); i++) {
			a[s.charAt(i)]++;
		}

		
		for (char ch = 'a'; ch <= 'z'; ch++) {
			int count = 0;
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == ch) {
					count++;
				}
			}
			if (count == 0) {
				System.out.println("it is not pangram");
				break;
			} else if (ch == 'z' && count >= 1) {
				System.out.println("it is panagram");
			}

		}

	}
}
