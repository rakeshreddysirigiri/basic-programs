package com.te.practise.multiThreading;

public class Example extends Thread {
	int count;

	public synchronized void run() {
		for (int i = 1; i <= 10000; i++) {
			count++;
		}
	}

	public static void main(String[] args) throws InterruptedException {

		Example example = new Example();
		Thread t1 = new Thread(example);
		Thread t2 = new Thread(example);
		t1.start();
		t2.start();

		t1.join();
		t2.join();
		System.out.println(example.count);
	}
}
