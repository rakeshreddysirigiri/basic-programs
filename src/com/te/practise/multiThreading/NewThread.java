package com.te.practise.multiThreading;

class UserThread extends Thread {
	public void run() {
		System.out.println("this is the used thread");
	}
}

public class NewThread {
	public static void main(String[] args) {
		System.out.println("program started");
		System.out.println(10);
		Thread currentThread = Thread.currentThread();
		System.out.println("Old Name : " + currentThread.getName());
		currentThread.setName("MYMAINTHREAD");
		System.out.println("ID :" + currentThread.getId());
		System.out.println("New Name : " + currentThread.getName());
		try {
			currentThread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("program ended");
		UserThread userThread = new UserThread();
		userThread.start();

	}
}
