package com.te.practise.multiThreading;

class MyThread implements Runnable {

	@Override
	public void run() {

		for (int i = 0; i <= 10; i++) {
			System.out.println(i);
		}
	}

}

class MyThread2 extends Thread {
	public void run() {
		for (int i = 11; i <= 20; i++) {
			System.err.println(i);
		}
	}
}

public class CreatingAThread implements Runnable {

	@Override
	public void run() {

		for (int i = 0; i <= 10; i++) {
			System.out.println(i);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		CreatingAThread c = new CreatingAThread();
		MyThread t = new MyThread();
		Thread t1 = new Thread(c);
		MyThread2 t2 = new MyThread2();
		t2.start();
		t2.join();
		t1.start();
		t1.join();
		for (int i = 50; i < 60; i++) {
			System.err.println(i);
		}

	}
}
