package com.te.practise.multiThreading2;

public class Company {

	int n;
	boolean f = false;

	public synchronized void produceItem(int n) throws InterruptedException {
		if (f == true) {
			wait();
		}
		this.n = n;
		System.out.println("Produced : " + n);
		f = true;
		notify();
	}

	public synchronized int consumeItem(int n) throws InterruptedException {
		this.n = n;
		if (f == false) {
			wait();
		}
		System.out.println("Consumed : " + n);
		f = false;
		notify();
		return this.n;
	}
}
