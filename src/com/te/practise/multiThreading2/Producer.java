package com.te.practise.multiThreading2;

public class Producer extends Thread {

	Company c;

	Producer(Company c) {
		this.c = c;
	}

	public void run() {
		int i = 1;
		while (true) {
			
			try {
				this.c.produceItem(i);
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;

		}
	}
}
