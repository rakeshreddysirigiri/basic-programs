package com.te.practise.multiThreading2;

public class Consumer extends Thread {

	Company c;

	public Consumer(Company c) {
		this.c = c;
	}

	public void run() {
		int i = 1;
		while (true) {
			
			try {
				this.c.consumeItem(i);
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i++;
		}
	}
}
