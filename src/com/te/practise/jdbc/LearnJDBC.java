package com.te.practise.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LearnJDBC {

	public static void main(String[] args) throws ClassNotFoundException, SQLException  {

		String url = "jdbc:mysql://localhost:3306/practise?user=root&password=@Rajeshreddy20";
		Class.forName("com.mysql.cj.jdbc.Driver");
	     Connection connection = DriverManager.getConnection(url);
	     Statement statement = connection.createStatement();
	     ResultSet query = statement.executeQuery("select * from employee");
	     
	     while(query.next()) {
	    	 for(int i=1;i<query.getMetaData().getColumnCount();i++) {
	    		 if(i>1) System.out.print(" ");
	    		 else System.out.println();
	    		 System.out.print(query.getString(i));
	    	 }
	    	 
	     }	
	}
}
