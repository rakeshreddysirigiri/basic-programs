package com.te.practise.map;

import java.net.InetAddress;
import java.net.UnknownHostException;

interface Employe {

	public static String name = "Rakesh";

	public void main();
}
 
interface Admin extends Employe {
	public static String name = "Reddy";
}

public class Main implements Admin {

	public static void main(String[] args) {
		Admin admin = new Main();
		admin.main();
	}

	@Override
	public void main() {
		InetAddress ip;
		try {
			ip = InetAddress.getByName("192.168.187.133");
			System.out.println(ip.getHostName());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}
}
