package com.te.practise.map;

import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class PractiseMap {

	public static void main(String[] args) {

		Comparator<Map.Entry<Integer, String>> comp = (e1, e2) -> {
			return e1.getValue().compareToIgnoreCase(e2.getValue());
		};

		Map<Integer, String> map = new TreeMap<Integer, String>();
		map.put(1, "one");
		map.put(2, "two");
		map.put(3, "three");

		Set<Entry<Integer, String>> collect = map.entrySet().stream()
				.sorted((e1, e2) -> e1.getValue().compareToIgnoreCase(e2.getValue())).collect(Collectors.toSet());
		System.out.println(collect);

		List<String> set = new LinkedList<String>();
		set.add("1");
		set.add("2");
		set.add("3");
		set.add("4");
		set.add("7");
		set.add("6");

		List<String> list = set.stream().map(e -> e + "1").collect(Collectors.toList());
		System.out.println(list);

		Set<Employee> e = new HashSet<Employee>();

		e.add(new Employee("A", 24, 10.00));
		e.add(new Employee("B", 26, 20.00));
		e.add(new Employee("C", 23, 30.00));
		e.add(new Employee("D", 21, 40.00));
		e.add(new Employee("E", 28, 50.00));

		e.stream().forEach(b -> b.setSalaray(b.getSalaray() * 1.25));
		e.stream().sorted((e1, e2) -> e1.getName().compareTo(e2.getName())).forEach(System.out::println);
		System.out.println(e);

	}

}
