package com.te.practise.logical.ScenarioBased.supermarket;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {
	public static void main(String[] args) {

		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList.add(new Employee(111, "Jiya Brein", 32, "Female", "HR", 2011, 25000.0));
		employeeList.add(new Employee(122, "Paul Niksui", 25, "Male", "Sales And Marketing", 2015, 13500.0));
		employeeList.add(new Employee(133, "Martin Theron", 29, "Male", "Infrastructure", 2012, 18000.0));
		employeeList.add(new Employee(144, "Murali Gowda", 28, "Male", "Product Development", 2014, 32500.0));
		employeeList.add(new Employee(155, "Nima Roy", 27, "Female", "HR", 2013, 22700.0));
		employeeList.add(new Employee(166, "Iqbal Hussain", 43, "Male", "Security And Transport", 2016, 10500.0));
		employeeList.add(new Employee(177, "Manu Sharma", 35, "Male", "Account And Finance", 2010, 27000.0));
		employeeList.add(new Employee(188, "Wang Liu", 31, "Male", "Product Development", 2015, 34500.0));
		employeeList.add(new Employee(199, "Amelia Zoe", 24, "Female", "Sales And Marketing", 2016, 11500.0));
		employeeList.add(new Employee(200, "Jaden Dough", 38, "Male", "Security And Transport", 2015, 11000.5));
		employeeList.add(new Employee(211, "Jasna Kaur", 27, "Female", "Infrastructure", 2014, 15700.0));
		employeeList.add(new Employee(222, "Nitin Joshi", 25, "Male", "Product Development", 2016, 28200.0));
		employeeList.add(new Employee(233, "Jyothi Reddy", 27, "Female", "Account And Finance", 2013, 21300.0));
		employeeList.add(new Employee(244, "Nicolus Den", 24, "Male", "Sales And Marketing", 2017, 10700.5));
		employeeList.add(new Employee(255, "Ali Baig", 23, "Male", "Infrastructure", 2018, 12700.0));
		employeeList.add(new Employee(266, "Sanvi Pandey", 26, "Female", "Product Development", 2015, 28900.0));
		employeeList.add(new Employee(277, "Anuj Chettiar", 31, "Male", "Product Development", 2012, 35700.0));
		employeeList.add(new Employee(278, "Rakesh", 21, "Male", "Product Development", 2012, 35700.1));

		// how many male and female employees in organization
		Map<String, Long> maleAndFemale = employeeList.stream()
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
		System.out.println("no of male and female employees are :" + maleAndFemale);

		// list all the departments in the organization
		employeeList.stream().map(e -> e.getDepartment()).distinct().forEach(System.out::println);

		// print the average age of the employees
		Map<String, Double> averageOfEmployees = employeeList.stream()
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingInt(Employee::getAge)));
		System.out.println("Average age  of male and female employees :" + averageOfEmployees);

		// Details of highest paid employee in the organization
		Optional<Employee> max = employeeList.stream()
				.collect(Collectors.maxBy(Comparator.comparingDouble(Employee::getSalary)));
		System.out.println("Details of highest paid employee:" + max.get());

		// name of the employees joined after 2015
		employeeList.stream().filter(e -> e.getYearOfJoining() > 2015).forEach(e -> System.out.println(e.getName()));

		// no of employees in each department
		Map<String, Long> noOfEmlpoyessInEachDepartment = employeeList.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));
		System.out.println("no of employees in each department :" + noOfEmlpoyessInEachDepartment);

		// average salary of each department
		Map<String, Double> averageSalaryOfEachDepartment = employeeList.stream().collect(
				Collectors.groupingBy(Employee::getDepartment, Collectors.averagingDouble(Employee::getSalary)));
		System.out.println("Average salary of each department :" + averageSalaryOfEachDepartment);

		// print the details of youngest employee in the product and development
		Optional<Employee> youngestEmployee = employeeList.stream()
				.filter(e -> e.getDepartment().equals("Product Development"))
				.collect(Collectors.minBy(Comparator.comparingInt(Employee::getAge)));
		System.out.println("Youngest employee" + youngestEmployee.get());

		// employee with highest experience
		Optional<Employee> mostExperienced = employeeList.stream()
				.collect(Collectors.minBy(Comparator.comparingInt(Employee::getYearOfJoining)));
		System.out.println("Highest Experience :" + mostExperienced);

		// average salary of male and female employees
		Map<String, Double> averageSalaryOfMaleAndFemale = employeeList.stream()
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingDouble(Employee::getSalary)));
		System.out.println("Average Salary of male and female :" + averageSalaryOfMaleAndFemale);

		// names of the employees in the department
		Map<String, List<Employee>> employeesInEachDepartment = employeeList.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment));

		// how many male and female employees are there in marketing team ]
		Map<String, Long> countOfMaleAndFemaleInSales = employeeList.stream()
				.filter(e -> e.getDepartment().equals("Sales And Marketing"))
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
		System.out.println("Count of Male and Female in Sales :" + countOfMaleAndFemaleInSales);

		// print average and total salary of given to the employees in the organization
		DoubleSummaryStatistics totalSalary = employeeList.stream()
				.collect(Collectors.summarizingDouble(Employee::getSalary));
		System.out.println("Total salary of the organization :" + totalSalary.getSum());
		System.out.println("Average salary of the organization :" + totalSalary.getAverage());

		// Separate the employees whose age is greatet
		Map<Boolean, List<Employee>> seperatedEmployees = employeeList.stream()
				.collect(Collectors.partitioningBy(e -> e.getAge() > 25));
		System.out.println(seperatedEmployees);

		// who is the oldest employee and what is his age and departmentname
		Optional<Employee> oldestEmployee = employeeList.stream()
				.min((e1, e2) -> e1.getYearOfJoining() - e2.getYearOfJoining());
		System.out.println("name :" + oldestEmployee.get().getName() + " Age : " + oldestEmployee.get().getAge()
				+ " Department :" + oldestEmployee.get().getDepartment());

	}

}
