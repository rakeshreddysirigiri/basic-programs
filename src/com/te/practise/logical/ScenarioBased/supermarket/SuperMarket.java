package com.te.practise.logical.ScenarioBased.supermarket;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class SuperMarket {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		List<Item> items = new ArrayList<Item>();
		items.add(new Item("F1", "Apple", 230.25));
		items.add(new Item("V1", "Okra", 35.00));
		items.add(new Item("V99", "Potato", 23.70));

		System.out.println("List of items \n");
		items.forEach(e -> System.out.println(e));

		System.out.println("sorted based on their price \n");
		items.stream().sorted((e1, e2) -> e1.getPrice().compareTo(e2.getPrice())).forEach(e -> System.out.println(e));

		System.out.println("Searching the items \n");
		String barcode = sc.next();
		Optional<Item> findFirst = items.stream().filter(e -> e.getBarcode().equals(barcode)).findFirst();
		if (findFirst.isPresent()) {
			System.out.println(findFirst.get());
		} else
			throw new RuntimeException("Item not available");

	}
}
