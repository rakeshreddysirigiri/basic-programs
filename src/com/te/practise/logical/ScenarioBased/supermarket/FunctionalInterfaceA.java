package com.te.practise.logical.ScenarioBased.supermarket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FunctionalInterface
interface AddMethod {

	public int run(int a, int b);
}

public class FunctionalInterfaceA {

	public static List<Integer> integer = new ArrayList<Integer>();

	public static void main(String[] args) {

		String s = "rakesh reddy";
		String replace = s.replace("r", "a");
		System.out.println(replace);

		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "Rakesh");
		map.put(1, replace);
		System.out.println(map);

		
		List<Integer> list = new ArrayList<Integer>();

		Integer[] array = new Integer[5000];
		for (int i = 4999; i >= 0; i--) {
			array[i] = i;
		}

		Integer[] array2 = Arrays.asList(array).stream().sorted().toArray(Integer[]::new);
		System.out.println(Arrays.toString(array2));

		FunctionalInterfaceA a = new FunctionalInterfaceA();
		list.stream().forEach(a::add);
		System.out.println(FunctionalInterfaceA.integer);

		AddMethod method = ABC::run;
		System.out.println(method.run(3, 4));
		
		
	   

	}

	public void add(Integer a) {
		if (!integer.contains(a)) {
			integer.add(a);
		}
	}

}

class ABC {

	public static int run(int a, int b) {
		return a + b;
	}

}
