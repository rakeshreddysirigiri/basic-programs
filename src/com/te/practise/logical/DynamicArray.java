package com.te.practise.logical;

import java.util.Arrays;

public class DynamicArray<T> {

	Object[] array;
	int size;

	public DynamicArray() {
		size = 0;
		array = new Object[1];
	}

	public int getSize() {
		return array.length;
	}

	@SuppressWarnings("unchecked")
	public T get(int index) {
		return (T) array[index];
	}

	public void put(Object element) {
		ensureCapacity(size + 1);
		array[size++] = element;
	}

	private void ensureCapacity(int minCapacity) {
		int oldCapacity = getSize();
		if (minCapacity > oldCapacity) {
			int newCapacity = oldCapacity * 2;
			array = Arrays.copyOf(array, newCapacity);
		}
	}

	public static void main(String[] args) {
		DynamicArray<Integer> da = new DynamicArray<Integer>();
		da.put(1);
		System.out.println(da.get(0));
		da.put(2);
		System.out.println(da.get(1));
		da.put(3);
		System.out.println(da.get(2));
		da.put(4);
		System.out.println(da.get(3));
	}
}
