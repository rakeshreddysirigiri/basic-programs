package com.te.practise.logical;

import java.util.Arrays;

public class Anagram {
	public static void main(String[] args) {
		String str1 = "FRIED";
		String str2 = "FIRED";

		if (str1.length() != str2.length()) {
			System.out.println("not an anagram");
		} else {
			char[] a = str1.toCharArray();
			char[] b = str2.toCharArray();
			for (int i = 0; i < str1.length(); i++) {
				for (int j = i + 1; j < str1.length(); j++) {
					if (a[i] > a[j]) {
						char ch = a[i];
						a[i] = a[j];
						a[j] = ch;
					}
					if (b[i] > b[j]) {
						char ch = b[i];
						b[i] = b[j];
						b[j] = ch;
					}
				}
			}
			if (Arrays.toString(a).equals(Arrays.toString(b))) {
				System.out.println("Anagram");
			} else {
				System.out.println("not an anagram");
			}

		}
	}
}
