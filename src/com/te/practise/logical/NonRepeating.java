package com.te.practise.logical;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NonRepeating {

	public static void main(String[] args) {

		String str = "qwertyuiopasdfghjkllkjhgfdxszapoiuytrew";
		int[] arr = new int[256];
		for (int i = 0; i < str.length(); i++) {
			arr[str.charAt(i)]++;
			System.out.println(str.charAt(i) + " " + arr[str.charAt(i)]);
		}
		System.out.println("Non Repeating characters are");
		for (int i = 0; i < 256; i++) {
			if (arr[i] == 1) {
				System.out.println((char) i);
				break;
			}
		}

		// using stream
		List<Character> charecters = new ArrayList<Character>();
		for (Character character : str.toCharArray()) {
			charecters.add(character);
		}

		System.out.println(
				(charecters.stream().filter(e -> Collections.frequency(charecters, e) == 1).findFirst().get()));
	}

}
