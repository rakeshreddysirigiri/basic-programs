package com.te.practise.logical;

interface A{
	
	@Override
	public String toString();
	
	@Override
	public int hashCode();	
}

class B implements A{
	
	
	public static void main(String[] args) {
		A a = new B();
			System.out.println(a instanceof A);
	}
	
}
public class ArraysSum {

	public static void main(String[] args) { 

		ArraysSum sum = new ArraysSum();
		if (!(sum instanceof ArraysSum)) {
			System.out.println(sum);
		}
             
		int[] a = new int[] { 1, 2, 3, 4, 5 };
		for (int i = 0; i < a.length; i++) {
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] + a[j] == 5) {
					System.out.println(a[i] + "," + a[j]);
				}
			}
		}
	}	
}
