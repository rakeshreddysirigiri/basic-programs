package com.te.practise.logical;

public class LargestElement {
	public static void main(String[] args) {
		int[] a = new int[] { 1, 2, 12, 3, 4, 5, 6, 7, 8, 9 };
		int max = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
			}
		}
		System.out.println(max);
	}
}
