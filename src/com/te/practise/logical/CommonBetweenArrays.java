package com.te.practise.logical;

public class CommonBetweenArrays {

	public static void main(String[] args) {
		int[] a = new int[] { 1, 4, 7, 9, 2, 7 };
		int[] b = new int[] { 1, 7, 3, 4, 5, 7 };

		for (int i = 0; i < a.length; i++) {
			int count = 0;

			for (int j = i + 1; j < a.length; j++) {
				if (a[i] == a[j]) {
					a[j] = -1;
				}
			}

			for (int j = 0; j < b.length; j++) {
				if (a[i] == b[j]) {
					count++;
				}
			}

			if (count >= 1) {
				System.out.println(a[i]);
			}
		}
	}
}
