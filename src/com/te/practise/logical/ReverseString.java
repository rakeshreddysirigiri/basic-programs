package com.te.practise.logical;

import java.util.Arrays;

public class ReverseString {

   public ReverseString rev= new ReverseString();
	
	public static void main(String[] args) {
		
		char[] charArray = "JAVA JSP ANDROID".toCharArray();

		char[] ch = new char["JAVA JSP ANDROID".length()];

		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == ' ') {
				ch[i] = '_';
			}
		}


		int j = ch.length - 1;
		for (int i = 0; i < ch.length; i++) {
			if (charArray[i] != ' ') {
				if (ch[j] == '_') {
					j--;
				}
				ch[j] = charArray[i];
				j--;
			}
		}

		System.out.println(String.valueOf(ch).replace("_", " "));
		System.out.println(Arrays.toString(charArray));
		System.out.println(Arrays.toString(ch));
	}
}
