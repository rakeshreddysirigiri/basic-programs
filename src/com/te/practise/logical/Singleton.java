package com.te.practise.logical;

class Employee {

	private String name;
	private int id;

	private String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	private int getId() {
		return id;
	}

	private void setId(int id) {
		this.id = id;
	}

	private static void setEmployee(Employee employee) {
		Employee.employee = employee;
	}

	private static  Employee employee;

	private Employee(String name, int id) {
		this.name = name;
		this.id = id;
	}

	public static Employee getEmployee() {
		if (employee == null) {
			employee = new Employee("Rakesh", 1);
			return employee;
		}
		return employee;
	}
}

public class Singleton {
	public static void main(String[] args) {
		Employee employee = Employee.getEmployee();
		Employee employee1 = Employee.getEmployee();
		System.out.println(employee == employee1);

	}
}
