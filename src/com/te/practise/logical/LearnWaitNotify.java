package com.te.practise.logical;
class MyThread extends Thread
{
	int total=0;
	@Override
	public synchronized void run() {
		for (int i = 0; i <5; i++) {
			total=total+i;
		}
		//this.notify();
	}
}
public class LearnWaitNotify {
   public static void main(String[] args) throws InterruptedException {
	MyThread t=new MyThread();
	t.start();
//	synchronized (t) {
//		t.wait();
//	}
	System.out.println(t.total);
}
}
