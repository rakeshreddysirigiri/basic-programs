package com.te.practise.logical;

public class SquareRoot {
	public static void main(String[] args) {
		int i = 27;
		for (int j = 0; j < i / 3; j++) {
			if (j * j * j == i) {
				System.out.println(j);
			}
		}
	}
}
