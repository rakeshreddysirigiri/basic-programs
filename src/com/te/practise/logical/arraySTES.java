package com.te.practise.logical;

import java.util.Arrays;

class Object5 {

	public synchronized void method1(Object6 obj) {
		System.out.println("thread entered object5 synchronized method");
		obj.method1(this);
		System.out.println("object5 ends");
	}
}

class Object6 {
	public synchronized void method1(Object5 obj) {
		System.out.println("thread entered object6 synchronized method");
		obj.method1(this);
		System.out.println("object6ends");
	}
}

public class arraySTES {

	public static void main(String[] args) throws InterruptedException {

		Object5 obj5 = new Object5();
		Object6 obj6 = new Object6();

		Thread t8 = new Thread(() -> obj5.method1(obj6));
		Thread t9 = new Thread(() -> obj6.method1(obj5));
		t8.start();
		t9.start();

		int[] a = new int[100];
		int[] b = new int[10];

		b = a;
		System.out.println(Arrays.toString(b));

		StringBuffer str = new StringBuffer("Rakesh reddy");

		int c = 10;
		int d = 20;

		c = c + d;
		d = c - d;
		c = c - d;
		System.out.println(c + " " + d);

		String s1 = "Rakesh";
		String s2 = "Reddy";

		s1 = s1 + s2;
		s2 = s1.substring(0, s1.length() - s2.length());
		s1 = s1.substring(s2.length());
		System.out.println(s1 + " " + s2);

		Thread t1 = new Thread() {
			public void run() {
				for (int i = 0; i <= 10; i++) {
					System.out.println(i);
				}
			}
		};

		Thread t3 = new Thread() {
			public void run() {
				try {
					t1.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for (int i = 21; i <= 30; i++) {
					System.out.println(i);
				}
			}
		};

		Thread t2 = new Thread() {
			public void run() {
				for (int i = 11; i <= 20; i++) {
					try {
						t3.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(i);
				}
			}
		};

		/*
		 * t1.start(); t2.start(); t3.start();
		 */

	}
}
