package com.te.practise.logical;

public class DuplicateElementsInString {

	public static void main(String[] args) {
		String s = "Java";
		String lowerCase = s.toLowerCase();
		for (char ch = 'a'; ch <= 'z'; ch++) {
			int count = 0;
			for (int i = 0; i < lowerCase.length(); i++) {
				if (lowerCase.charAt(i) == ch) {
					count++;
				}
			}
			if (count > 1) {
				System.out.println(ch);
			}
		}

		String s1 = "google";
		char[] charArray = s1.toCharArray();
		s1.toCharArray();
		String b = "";
		for (int i = 0; i < charArray.length; i++) {
			for (int j = i + 1; j < charArray.length;j++) {
				if (charArray[i]==charArray[j]) {
					charArray[j]='0';
				}
			}
			if (charArray[i]!='0') {
				b = b + s1.charAt(i);

			}
		}
		System.out.println(b);

	}
}
