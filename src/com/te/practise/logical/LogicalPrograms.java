package com.te.practise.logical;

public class LogicalPrograms {

	public static void main(String[] args) {
	
		String input = "aaa bbb ccc dd abcd";
		for (char ch = 'a'; ch <= 'z'; ch++) {
			int count = 0;
			for (int i = 0; i < input.length(); i++) {
				char ch1 = input.charAt(i);
				if (ch == ch1) {
					count++;
				}
			}
			if (count >= 1) {
				System.out.print(ch + ":" + count + " ");
			}
		}

	}
}
/*
 * output : a: 4 b:4 c:4 d:3
 */