package com.te.practise.logical;

import java.util.Map;
import java.util.stream.Collectors;

public class OccurenceOfCharecter {
	
	public static void main(String[] args) {
		String s1 = "rakeshreddysirigiri";
		char[] charArray = s1.toCharArray();
		for (char ch = 'a'; ch < 'z'; ch++) {
			int count = 0;
			for (int i = 0; i < charArray.length; i++) {
				if (ch == charArray[i]) {
					count++;
				}
			}
			if (count > 0) {
				System.out.println(ch + "-" + count);
			}
		}

		String s = "google";
		String b = "";
		for (int i = 0; i < s.length(); i++) {
			int count1 = 0;
			for (int j = i + 1; j < s.length(); j++) {
				if (s.charAt(i) == s.charAt(j)) {
					count1++;
				}
			}

			for (int j = 0; j < i; j++) {
				if (s.charAt(i) == s.charAt(j)) {
					count1++;
				}
			}
			if (count1 == 0) {
				b = b + s.charAt(i);
			}
		}

		System.out.println(b);

		System.out.println(Integer.parseInt("1234"));

		s1.chars().mapToObj(c -> (char) c).distinct().collect(Collectors.toList()).forEach(e -> System.out.print(e));
		Map<Character, Long> collect = s1.chars().mapToObj(c -> (char) c)
				.collect(Collectors.groupingBy(c -> c, Collectors.counting()));
		System.out.println(collect);

		System.out.println((char) s1.chars().max().getAsInt());
	}
}
