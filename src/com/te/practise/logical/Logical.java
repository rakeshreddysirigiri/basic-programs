package com.te.practise.logical;


public class Logical {

	public static void main(String[] args) {
		Logical.m1(null);
	}
	
	public static void m1(Object o) {
		System.out.println("  " + o);
	}

	public static void m1(Integer s) {
		System.out.println(s);
	}

}
