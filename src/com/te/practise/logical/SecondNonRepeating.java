package com.te.practise.logical;

public class SecondNonRepeating {

	public static void main(String[] args) {

		String s = "rakeshreddysirigiri";
		int pos = 0;
		for (int i = 0; i < s.length(); i++) {

			int count = 0;
			for (int j = i + 1; j < s.length(); j++) {
				if (s.charAt(i) == s.charAt(j)) {
					count++;
				}
			}
			for (int j = 0; j < i; j++) {
				if (s.charAt(i) == s.charAt(j)) {
					count++;
				}
			}
			if (count == 0) {
				pos++;
				if (pos == 2) {
					System.out.println(s.charAt(i));
					break;
				}
			}
		}
	}

}
