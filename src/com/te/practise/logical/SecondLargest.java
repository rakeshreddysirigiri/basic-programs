package com.te.practise.logical;

public class SecondLargest {
	public static void main(String[] args) {
		int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7 ,12};
		int largest = 0;
		int secondLargest = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] > largest) {
				secondLargest = largest;
				largest = a[i];

			}
			if (a[i] < largest && a[i] >= secondLargest) {
				secondLargest = a[i];
			}
		}
		System.out.println(secondLargest);

	}
}
