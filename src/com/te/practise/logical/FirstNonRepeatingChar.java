package com.te.practise.logical;

public class FirstNonRepeatingChar {

	public static void main(String[] args) {
		String s = "qwertyuiopaaddfghjklzxcvbnmpoiuytrewq";
		for (int i = 0; i < s.length(); i++) {
			int count = 0;
			for (int j = i + 1; j < s.length(); j++) {
				if (s.charAt(i) == s.charAt(j)) {
					count++;
					break;
				}
			}
			for (int j = 0; j < i; j++) {
				if (s.charAt(i) == s.charAt(j)) {
					count++;
					break;
				}
			}
			if (count == 0) {
				System.out.println(s.charAt(i));
				break;
			}
		}
	}
}
