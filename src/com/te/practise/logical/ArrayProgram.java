package com.te.practise.logical;

public class ArrayProgram {
	public final static void main(String... gsdr) {
		int[] a= new int[] { 0, 1, 2, 2, 2, 4, 4, 1 };
		int result = -1;
		int maxCount = 0;
		for (int i = 0; i < a.length; i++) {
			int count = 0;
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] == a[j]) {
					count++;
				}
			}
			if (a[i] % 2 == 0 && a[i] != 0) {

				if (maxCount < count) {
					maxCount = count;
					result = a[i];
				}
				else if (maxCount == count) {
					if (result > a[i]) {
						result = a[i];
					}
				}else if (maxCount == 0) {
					result = a[i];
				}

			}
		}
		System.out.println(result);

	}
}
