package com.te.practise.logical;

class Object1 {
	synchronized public void object1(Object2 obj) {
		System.out.println("using synchronized block of object1");
		obj.object2(this);
		System.out.println("object1 has completely executed");
	}
}

class Object2 {
	synchronized public void object2(Object1 obj) {
		System.out.println("using synchronized block of object2");
		obj.object1(this);
		System.out.println("object2 has completely executed");
	}
}

public class DeadLock {

	public static void main(String[] args) {

		Object1 obj1 = new Object1();
		Object2 obj2 = new Object2();

		Thread thread1 = new Thread(() -> obj1.object1(obj2));
		Thread thread2 = new Thread(() -> obj2.object2(obj1));
		thread1.start();
		thread2.start();
	}
}
