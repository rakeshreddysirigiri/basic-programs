package com.te.practise.logical;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StringProgram {

	public static void main(String[] args) {

		final List<Integer> list = new ArrayList<Integer>();
		list.add(10);
		list.add(20);
		list.add(30);

		list.stream().toArray(Integer[]::new);
		System.out.println(list.remove(1));
		Iterator<Integer> iterator = list.iterator();

		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}

		String s = "I am am Java Java Developer";
		String[] s2 = s.split(" ");
		for (int i = 0; i < s2.length; i++) {
			int count = 1;
			for (int j = i + 1; j < s2.length; j++) {
				if (s2[i].equals(s2[j])) {
					count++;
				}
			}
			if (count > 1) {
				System.out.println(s2[i]);
			}
		}
		Arrays.asList(s2).stream().filter(e -> Collections.frequency(list, e) > 1).forEach(System.out::println);

	}
}
