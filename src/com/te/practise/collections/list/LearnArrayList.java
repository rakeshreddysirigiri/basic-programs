package com.te.practise.collections.list;

import java.util.ArrayList;
import java.util.List;

public class LearnArrayList {

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");

		
		list.add(3,"e");
		
		System.out.println(list.get(1));
		
		System.out.println(list);

	}
}
