package com.te.practise.collections.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LearnMap {
	public static void main(String[] args) {
            Map<String, Integer> hashMap = new HashMap<String, Integer>();
            
            hashMap.put("A",1);
            hashMap.put("B",1);
            hashMap.put("C",1);
            hashMap.put("D",1);
            hashMap.put("E",1);
            hashMap.put("F",1);
             
            /*
             * used to replace the value mapped to the specified key
             */
            hashMap.replace("A",2);
            
            /*
             * used the retrieve the value mapped to the specified key
             * and returns null if no mapping found
             */
            hashMap.get("A");
            
            /*
             * used to remove the entry object associated with the key in the map
             */
            hashMap.remove("A");
            /*
             * returns all the keys in the map object into a set type
             */
            Set<String> keySet = hashMap.keySet();
            
            /*
             *  removes the entry object only if the key is mapped to specified value
             */
            hashMap.remove("B",2);
            
            
            
            System.out.println(keySet);
            
           System.out.println(hashMap);
            
	}
}
