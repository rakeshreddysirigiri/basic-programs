package com.te.practise.collections.set;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class LearnSet {

	public static void main(String[] args) {

		Comparator<String> comp = (a, b) -> a.compareTo(b);

		Set<String> set = new TreeSet<String>(comp);

		set.add("a");
		set.add("b");
		set.add("c");
		set.add("d");
		set.add("g");
		set.add("f");
		set.add("e");
		
		System.out.println(set);

	}
}
