package com.te.practise.streams4;

class A {
	public A returnA() throws Exception {
		return this;
	}
}

class B extends A {

	@Override
	public B returnA() throws RuntimeException {
		return this;
	}
}

public class ABC {

	public static void main(String[] args) {

		B b = new B();
		B returnA = b.returnA();
	}

}
