package com.te.practise.streams4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {

		List<Student> students = new ArrayList<Student>();

		students.add(new Student("Peter", 1, "English", 97.0));
		students.add(new Student("Mark", 1, "Maths", 28.0));
		students.add(new Student("Andrew", 1, "Science", 71.0));
		students.add(new Student("mark", 1, "Maths", 58.0));
		students.add(new Student("Peter", 1, "Science", 51.0));
		students.add(new Student("Harsh", 1, "English", 97.0));
		students.add(new Student("Rakesh reddy", 1, "English", 98.0));

		Map<Boolean, List<Student>> collect = students.stream()
				.collect(Collectors.partitioningBy(e -> e.getPercentage() > 60.0));
		collect.entrySet().stream().filter(e -> e.getKey()).forEach(System.out::println);

		students.stream().sorted((e1, e2) -> (int) e2.getPercentage() - (int) e1.getPercentage()).limit(3)
				.forEach(System.out::println);

		Map<String, Double> collect2 = students.stream().collect(
				Collectors.toMap(Student::getName, Student::getPercentage, (e1, e2) -> e1, LinkedHashMap::new));
		System.out.println(collect2);

		List<String> collect3 = students.stream().map(e -> e.getSubject()).distinct().collect(Collectors.toList());
		System.out.println(collect3);

		DoubleSummaryStatistics collect4 = students.stream()
				.collect(Collectors.summarizingDouble(Student::getPercentage));
		System.out.println(collect4.getMax());
		System.out.println(collect4.getAverage());
		System.out.println(collect4.getMin());

		long count = students.stream().count();
		System.out.println(count);

		Map<String, List<Student>> collect5 = students.stream().collect(Collectors.groupingBy(Student::getSubject));
		System.out.println(collect5);

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(1, "peter", 21, "MALE", "IT", 2021, 245793.00));
		employees.add(new Employee(1, "Mark", 22, "MALE", "Admin", 2020, 245465.00));
		employees.add(new Employee(1, "Andrew", 21, "MALE", "IT", 2021, 41615.00));
		employees.add(new Employee(1, "Anna", 21, "FEMALE", "Reception", 2021, 44541.00));
		employees.add(new Employee(1, "daniel", 21, "MALE", "Admin", 2021, 54165.00));
		employees.add(new Employee(1, "Sara", 21, "FEMALE", "Reception", 2021, 65455.00));
		employees.add(new Employee(1, "peter", 21, "MALE", "IT", 2021, 795515.00));

		Map<String, Long> collect6 = employees.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));
		System.out.println(collect6);

		Map<String, Double> collect7 = employees.stream()
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingDouble(Employee::getSalary)));
		System.out.println(collect7);

		Optional<Employee> max = employees.stream().max((e1, e2) -> (int) e2.getSalary() - (int) e2.getSalary());
		System.out.println(max.get());

		Optional<Employee> collect8 = employees.stream()
				.collect(Collectors.minBy(Comparator.comparingInt(Employee::getYearOfJoining)));
		System.out.println(collect8.get());

		Comparator<Employee> comp = (a, b) -> b.getAge() - a.getAge();

		Set<Employee> set = new TreeSet<Employee>(comp);

		Comparator<String> comp1 = (a, b) -> a.compareTo(b);

		Map<String, Employee> map = new TreeMap<String, Employee>(comp1);

		employees.stream().forEach(e -> e.setSalary(10000));   

		employees.forEach(System.out::println);

		employees.stream().sorted((a, b) -> b.getAge() - a.getAge());
		

	}
}
